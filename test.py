#!/usr/bin/python

import os
import json
import boto3

def main():
  with open ('/home/dan/work/dev/dynamo_config.json', 'r') as f:
    conf = json.load(f)
    args = conf['Values']

  profile = args['profile']
  region = args['region']
  table = args['table']
  path = args['path']

  dev = boto3.session.Session(profile_name = profile)
  dynamodb = dev.resource('dynamodb', region_name = region)
  table = dynamodb.Table(table)

  s = 0
  num = len(os.listdir(path))
  for filename in os.listdir(path):
    with open (path + filename, 'r') as f:
        i_dict = json.load(f)

    try:   
      inst_id = i_dict['inst_ID']
      acc_id = i_dict['acc_ID']
      acc = dev.client('organizations').describe_account(AccountId=acc_ID).get('Account').get('Name')
      OS = i_dict['os']
      region = i_dict['region']
      i_name = i_dict['name']
      user = i_dict['user']

      response = table.put_item(
          Item={
              'Instance_ID': inst_id,
              'Account': acc,
              'Account_ID': acc_id,
              'OS': OS,
              'Region': region,
              'Name': i_name,
              'User': user
          }
      )
      if response['ResponseMetadata']['HTTPStatusCode'] == 200:
          s += 1

    except Exception:
      continue

  print("Successfully pushed " + str(s))
  print(str(num - s) + ' errors')


if __name__ == "__main__":
  main()